using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{
    
    public void ButonStar()
    {
        SceneManager.LoadScene("niveles");
    }// boton que lleva al selector de niveles
    public void ButonNv1()
    {
        SceneManager.LoadScene("juego1");       
    }// boton que lleva al nivel 1
    public void ButonNv2()
    {
        SceneManager.LoadScene("juego2");
    }// boton que lleva al nivel 2
    public void ButonNv3()
    {
        SceneManager.LoadScene("juego3");
    }// boton que lleva al nivel 3
    public void ButonNv4()
    {
        SceneManager.LoadScene("juego4");
    }// boton que lleva al nivel 4
    public void ButonNv5()
    {
        SceneManager.LoadScene("juego5");
    }// boton que lleva al nivel 5
    public void BotonMenu()
    {

        SceneManager.LoadScene("Menu");
    }// boton que lleva al menu de inicio
    public void Loser()
    {
        SceneManager.LoadScene("lose");
    }// boton que lleva a la escena de lose
    public void Retry()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }// boton que recarga los niveles
   public void GanarPantalla()
    {
        SceneManager.LoadScene("won");
    }// boton que lleva a la escena de won

}
