using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

public class Gamepicereturn : MonoBehaviour
{
    public int xIndex;
    public int yIndex;
    Board m_board;
    bool m_isMoving = false;
    public InterpType interpolation;
    public MatchValue matchValue;
    internal void SetCoord(int x, int y)
    {
        xIndex = x;
        yIndex = y;
    } // ubicador de la "coordenada" de las piezas
    internal void Init(Board board)
    {
        m_board = board;
    }// inicializador 
    internal void Move(int x, int y, float moveTime)
    {
        if (!m_isMoving)
        {
            StartCoroutine(MoveRoutine(x, y, moveTime));
        }
    }// moviento de las piezas
    IEnumerator MoveRoutine(int destX, int destY, float timeToMove)
    {
        Vector2 startPosition = transform.position;
        bool reacedDestination = false;
        float elapsedTime = 0f;
        m_isMoving = true;

        while (!reacedDestination)
        {
            if (Vector2.Distance(transform.position, new Vector2(destX, destY)) < 0.01f)
            {
                reacedDestination = true;

                if (m_board != null)
                {
                    m_board.PlaceGamePiece(this, destX, destY);
                }
                break;
            }

            elapsedTime += Time.deltaTime;
            float t = Mathf.Clamp(elapsedTime / timeToMove, 0f, 1f);

            switch (interpolation)
            {

                case InterpType.Linear:

                    break;

                case InterpType.EaseOut:
                    t = MathF.Sin(t * Mathf.PI * .5f);
                    break;

                case InterpType.Eseln:
                    t = 1 - MathF.Cos(t * MathF.PI * .5f);
                    break;

                case InterpType.SmoothStep:
                    t = t * t * (3 - 2 * t);
                    break;

                case InterpType.SmootherStep:
                    t = t * t * t * (t * (t * 6 - 15) + 10);
                    break;

            }

            transform.position = Vector2.Lerp(startPosition, new Vector2(destX, destY), t);
            yield return null;
        }
        m_isMoving = false;


    }// corutina del movimiento de las piezas
    public enum InterpType
    {
        Linear,
        EaseOut,
        Eseln,
        SmoothStep,
        SmootherStep
    }// etiquetas del tipo de movimiento
    public enum MatchValue
    {
        Blue,
        Cyan,
        Orange,
        Pink,
        Purple,
        Red,
        White,
        Yellow
    }// etiqueta del "color" de cada pieza
}
