using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour
{
    public int xIndex;
    public int yIndex;

    Board m_board;

    internal void Init(int x, int y, Board board)

    {

        xIndex = x;
        yIndex = y;
        m_board = board;
    }// ubicador de las "coordenadas" del tile y su inicializador con el board
    private void OnMouseEnter()
    {
        m_board.DragToTile(this);
    }// detector del paso del mause por encima
    private void OnMouseDown()
    {
        m_board.ClickedTile(this);
    }// detector del paso del mause por hacer click
    private void OnMouseUp()
    {
        m_board.ReleaseTile();
    }// detector del paso del mause por soltar el click
}