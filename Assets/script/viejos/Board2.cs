using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using TMPro;


public class Board2 : MonoBehaviour
{
    /*
    public int alto, ancho, borde;

    public GameObject prefiles;
    public GameObject[] bolas;

    public float swapTime = 0.5f, score = 0, time;

    public Tile[,] board;
    public Bola[,] matriz;

    [SerializeField] Tile inicioP, finalP;
    bool sepuedemover = true;

    public TextMeshProUGUI scoreP, timeP;
    public Camera cum;
    public ParticleSystem explosion;   
    public AudioSource objeto;
    public AudioClip sonidoNom, sonidoSlip;
    void Start()
    {
        matriz = new Bola[ancho, alto];
        CrBoard();
        Cumera();
        PiezasALlenarMatriz();
        score = 0;
       
    }
    private void Update()
    {
        Puntaje();
        MedidorDeTiempo();
    }
    void CrBoard()
    {
        board = new Tile[ancho, alto];
        for (int i = 0; i < ancho; i++)//x
        {
            for (int j = 0; j < alto; j++)//y
            {
                GameObject gb = Instantiate(prefiles);
                gb.name = "Tile(" + i + "," + j + ")";
                gb.transform.position = new Vector3(i, j, 0);
                gb.transform.parent = transform;

                Tile tile = gb.GetComponent<Tile>();
                //tile.board = this;
                tile.Comienzo(i, j);
                board[i, j] = tile;
                //board[x,y]
            }
        }
    }
    void Cumera()
    {
        cum.transform.position = new Vector3(((float)ancho / 2) - 0.5f, ((float)alto / 2) - 0.5f, -10);
        float al = ((float)alto / 2f) + borde;
        float an = (((float)ancho / 2f) + borde) / ((float)Screen.width / (float)Screen.height);
        if (al > an)
        {
            cum.orthographicSize = al;
        }
        else
        {
            cum.orthographicSize = an;
        }
    }
    GameObject PiezasAleatorias()
    {
        int nA = Random.Range(0, bolas.Length);
        GameObject go = Instantiate(bolas[nA]);
        return go;

    }
    public void PiezasPosicion(Bola gp, int x, int y)
    {
        gp.transform.position = new Vector3(x, y, 0f);
        gp.Coordenada(x, y);
        gp.GetComponent<Bola>().board = this;
        matriz[x, y] = gp;
        //matriz[x, y] = gp;
    }
    public void PiezasALlenarMatriz()
    {
        List<Bola> addpiece = new List<Bola>();
        for (int x = 0; x < ancho; x++)
        {
            for (int y = 0; y < alto; y++)
            {
                if (matriz[x,y]==null)
                {
                    Bola gamepiece = LenarmatrizAl(x, y);
                    addpiece.Add(gamepiece);
                }
            }
        }
        bool estallena=false;
        int interacciones=0;
        int interaccionesMax=20;
        while (!estallena)
        {
            List<Bola> coincidencia = Encontrartodaslascoincidencias();
            if (coincidencia.Count==0)
            {
                estallena = true;
                break;
            }
            else
            {
                coincidencia = coincidencia.Intersect(addpiece).ToList();
                ReemplazarConPiezas(coincidencia);
            }
            if (interacciones> interaccionesMax)
            {
                estallena = true;
            }
            interacciones++;
        }

    }
    void ReemplazarConPiezas(List<Bola> coincidencias)
    {
        foreach (Bola gamepiece in coincidencias)
        {
            ClearP(gamepiece.cordX, gamepiece.cordY);
            LenarmatrizAl(gamepiece.cordX, gamepiece.cordY);
        }
    }
    Bola LenarmatrizAl(int x, int y)
    {
        GameObject go = PiezasAleatorias();
        PiezasPosicion(go.GetComponent<Bola>(), x, y);
        return go.GetComponent<Bola>();
    }
    public void SetInicialMause(Tile ini)
    {

        if (inicioP == null)
        {
            inicioP = ini;
        }
    }
    public void SetEndMause(Tile endi)
    {
        if (inicioP != null && EsVecino(inicioP, endi) == true)
        {

            finalP = endi;
        }
    }
    public void Relase()
    {
        if (inicioP != null && finalP != null)
        {
            CambioPieza();

        }

    }
    IEnumerator Cambiopiezascorrutine(Tile inic, Tile endg)
    {
        if (sepuedemover)
        {
            sepuedemover = false;
        }
        Bola GpIn = matriz[inic.indicex, inic.indicey];
        Bola GpEnd = matriz[endg.indicex, endg.indicey];
        GpIn.MoverPieza(endg.indicex, endg.indicey, swapTime);
        GpEnd.MoverPieza(inic.indicex, inic.indicey, swapTime);

        yield return new WaitForSeconds(swapTime);
        List<Bola> Piezainicial = EncontrarCoincidenciasEn(GpIn.cordX, GpIn.cordY);
        List<Bola> PiezaFinal = EncontrarCoincidenciasEn(GpEnd.cordX, GpEnd.cordY);

        if (Piezainicial.Count == 0 && PiezaFinal.Count == 0)
        {
            GpIn.MoverPieza(inic.indicex, inic.indicey, swapTime);
            GpEnd.MoverPieza(endg.indicex, endg.indicey, swapTime);
            yield return new WaitForSeconds(swapTime);
            sepuedemover = true;
        }
        else
        {
            PiezaFinal = Piezainicial.Union(PiezaFinal).ToList();
            ClearAndFullBoard(PiezaFinal);
        }

        /*  ResaltarCoincidenciasEn(GpIn.cordX, GpIn.cordY);
          ResaltarCoincidenciasEn(GpEnd.cordX, GpEnd.cordY);
         ClearP(Piezainicial);
        ClearP(PiezaFinal);

        CollapseColum(Piezainicial);
        CollapseColum(PiezaFinal);
         
        finalP = null;
        inicioP = null;
        Instantiate(explosion, transform.position, explosion.transform.rotation);

    }
    void CambioPieza()
    {
        StartCoroutine(Cambiopiezascorrutine(inicioP, finalP));
        AudioSource.PlayClipAtPoint(sonidoSlip, gameObject.transform.position);
    }
    bool EsVecino(Tile iny, Tile endy)
    {
        if (Mathf.Abs(iny.indicex - endy.indicex) == 1 && iny.indicey == endy.indicey)
        {
            return true;
        }
        else
        {
            if (Mathf.Abs(iny.indicey - endy.indicey) == 1 && iny.indicex == endy.indicex)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
    bool Rango(int _x, int _y)
    {

        return (_x < ancho && _x >= 0 && _y < alto && _y >= 0);
    }
    List<Bola> EncontrarCoincidencias(int startX, int startY, Vector2 rangobusqueda, int valorMn = 3)
    {
        List<Bola> coincidencias = new List<Bola>();

        Bola piezainicial = null;
        if (Rango(startX, startY))
        {
            piezainicial = matriz[startX, startY];
        }
        if (Rango(startX, startY))
        {
            coincidencias.Add(piezainicial);
        }
        else
        {
            return null;
        }
        int siguientex;
        int siguientey;
        int valorMx = ancho > alto ? ancho : alto;

        for (int i = 1; i < valorMx - 1; i++)
        {
            siguientex = startX + (int)Mathf.Clamp(rangobusqueda.x, -1, 1) * i;
            siguientey = startY + (int)Mathf.Clamp(rangobusqueda.y, -1, 1) * i;
            if (!Rango(siguientex, siguientey))
            {
                break;
            }
            Bola siguientepieza = matriz[siguientex, siguientey];
            if (siguientepieza==null)
            {
                break;
            }
            else
            {
                if (piezainicial.color == siguientepieza.color && !coincidencias.Contains(siguientepieza))
                {
                    coincidencias.Add(siguientepieza);
                }
                else
                {
                    break;
                }
            }
            
        }
        if (coincidencias.Count >= valorMn)
        {
            return coincidencias;
        }
        return null;
    }
    List<Bola> Busquedavertical(int startX, int startY, int valorMn = 3)
    {
        List<Bola> arriba = EncontrarCoincidencias(startX, startY, Vector2.up, 2);
        List<Bola> abajo = EncontrarCoincidencias(startX, startY, Vector2.down, 2);

        if (arriba == null)
        {
            arriba = new List<Bola>();
        }
        if (abajo == null)
        {
            abajo = new List<Bola>();
        }
        var listascombinadas = arriba.Union(abajo).ToList();
        return listascombinadas.Count >= valorMn ? listascombinadas : null;
    }
    List<Bola> BusquedaHorizontal(int startX, int startY, int valorMn = 3)
    {
        List<Bola> derecha = EncontrarCoincidencias(startX, startY, Vector2.right, 2);
        List<Bola> izquierda = EncontrarCoincidencias(startX, startY, Vector2.left, 2);

        if (derecha == null)
        {
            derecha = new List<Bola>();
        }
        if (izquierda == null)
        {
            izquierda = new List<Bola>();
        }
        var listascombinadas = derecha.Union(izquierda).ToList();
        return listascombinadas.Count >= valorMn ? listascombinadas : null;
    }
    public void DetectarCoincidancias()
    {
        for (int i = 0; i < ancho; i++)
        {
            for (int j = 0; j < alto; j++)
            {
                List<Bola> horizontal = BusquedaHorizontal(i, j, 3);
                List<Bola> vertical = Busquedavertical(i, j, 3);
                if (horizontal == null)
                {
                    horizontal = new List<Bola>();
                }
                if (vertical == null)
                {
                    vertical = new List<Bola>();
                }
                var listascombinadas = horizontal.Union(vertical).ToList();
                if (listascombinadas.Count >= 3)
                {
                    Debug.Log(listascombinadas.Count);
                }
                
                foreach (Bola pieze in listascombinadas)
                {
                    Resltartile(pieze.cordX, pieze.cordY, pieze.GetComponent<SpriteRenderer>().color = pieze.GetComponent<SpriteRenderer>().color);
                }
            }
        }

    }
    private void Resltartile(int x, int y, Color col)
    {
        SpriteRenderer sr = board[x, y].GetComponent<SpriteRenderer>();
        sr.color = col;
    }
    private void ResaltarCoincidencias()
    {
        for (int i = 0; i < ancho; i++)
        {
            for (int j = 0; j < alto; j++)
            {
                ResaltarCoincidenciasEn(i,j);
            }
        }

    }
    private void ResaltarCoincidenciasEn(int x, int y)
    {
        var listascombinadas = EncontrarCoincidenciasEn(x, y);

        if (listascombinadas.Count>0)
        {
            foreach (Bola piece in listascombinadas)
            {
                Resltartile(piece.cordX, piece.cordY, piece.GetComponent<SpriteRenderer>().color);
            }
        }
    }
    List<Bola> EncontrarCoincidenciasEn(int x, int  y)
    {
        List<Bola> Horizontal = BusquedaHorizontal(x,y,3);
        List<Bola> Vertical = Busquedavertical(x,y,3);

        if (Horizontal==null)
        {
            Horizontal = new List<Bola>();
        }
        if (Vertical==null)
        {
            Vertical = new List<Bola>();
        }
        var listascombinadas = Horizontal.Union(Vertical).ToList();
        return listascombinadas;
    }
    List<Bola> Encontrartodaslascoincidencias()
    {
        List<Bola> todascoincidencias = new List<Bola>();
        for (int i = 0; i < ancho; i++)
        {
            for (int j = 0; j < alto; j++)
            {
                var coinidencias = EncontrarCoincidenciasEn(i, j);
                todascoincidencias = todascoincidencias.Union(coinidencias).ToList();
            }
        }

        return todascoincidencias ;
    }
    List<Bola> EncontrarTodasLasCoincidenciasEn(List<Bola> gamepieces, int minleng=3)
    {
        List<Bola> matches = new List<Bola>();
        foreach (Bola gp in gamepieces)
        {
            matches = matches.Union(EncontrarCoincidenciasEn(gp.cordX,gp.cordY)).ToList();
        }
        return matches;
    }
    void CleanBoard()
    {
        for (int i = 0; i < ancho; i++)
        {
            for (int j = 0; j < alto; j++)
            {
                ClearP(i, j);
            }
        }
    }
    void ClearP(int x, int y)
    {
        Bola pieceCleaner = matriz[x, y];
        if (pieceCleaner!=null)
        {
            matriz[x, y] = null;
            Destroy(pieceCleaner.gameObject);
            AudioSource.PlayClipAtPoint(sonidoNom, gameObject.transform.position);
            score++;
        }
    }
    void ClearP(List<Bola> gamepieces)
    {
        foreach (Bola gp in gamepieces)
        {
            if (gp!= null)
            {
                ClearP(gp.cordX, gp.cordY);
            }
            
        }
    }
    List<Bola> CollapseColum(int colmn,float tiempoclapso=0.1f)
    {
        List<Bola> movimientoP = new List<Bola>();       
        for (int i = 0; i < alto-1; i++)
        {
            if (matriz[colmn, i] == null)
            {
                for (int j = i+1; j < alto; j++)
                {
                    if (matriz[colmn, j] != null)
                    {
                        matriz[colmn, j].MoverPieza(colmn, i, tiempoclapso*(j-i));
                        matriz[colmn, i] = matriz[colmn, j];
                        matriz[colmn, j].Coordenada(colmn, i);
                        if (!movimientoP.Contains(matriz[colmn, i]))
                        {
                            movimientoP.Add(matriz[colmn, i]);
                        }
                        matriz[colmn, j] = null;
                        break;
                    }
                }           
            }
        }
        return movimientoP;
    }
    List<Bola> CollapseColum(List<Bola> gamepiece)
    {
        List<Bola> movimientoP = new List<Bola>();
        List<int> columstiempocolapso = GetColumn(gamepiece);
        foreach (int column in columstiempocolapso)
        {
            movimientoP = movimientoP.Union(CollapseColum(column)).ToList();
        }
        return movimientoP;
    }
    List<int> GetColumn(List<Bola> gamepieces)
    {
        List<int> columnasIndex = new List<int>();
        foreach (Bola gamepiece in gamepieces)
        {
            if (!columnasIndex.Contains(gamepiece.cordX))
            {
                columnasIndex.Add(gamepiece.cordX);
            }

        }
        return columnasIndex;
    }
   void ClearAndFullBoard(List<Bola> gamepieces)
    {
        StartCoroutine(ClearAndFullBoardRotine(gamepieces));

    }
    IEnumerator ClearAndFullBoardRotine(List<Bola> gamepieces)
    {
        
        yield return StartCoroutine(ClearAndFullColumn(gamepieces));
        yield return null;
        yield return StartCoroutine(RefullRoutine());


    }
    IEnumerator ClearAndFullColumn(List<Bola> gamepieces)
    {
        List<Bola> movinpiece = new List<Bola>();
        List<Bola> matches = new List<Bola>();
        bool isfinished = false;
        while (!isfinished)
        {
            ClearP(gamepieces);
            yield return new WaitForSeconds(.5f);
            movinpiece = CollapseColum(gamepieces);
            
           
            while(!iscolapse(gamepieces))
            {
                yield return new WaitForEndOfFrame();
            }
            matches = EncontrarTodasLasCoincidenciasEn(movinpiece);
            if (matches.Count == 0)
            {
                isfinished = true;
                break;
            }
            else
            {
                yield return StartCoroutine(ClearAndFullColumn(matches));
            }
        }      
    }
    IEnumerator RefullRoutine()
    {
        PiezasALlenarMatriz();
        yield return null;
    }
    bool iscolapse(List<Bola> gamepiece)
    {
        foreach (Bola gp in gamepiece)
        {
            if (gp != null)
            {
                if (gp.transform.position.y-(float)gp.cordY > 0.01f)
                {
                    return false;
                }
            }
        }
        return true;
    }

    void Puntaje()
    {
        
        
        scoreP.text = "Score " + (int)score;
    }
    void MedidorDeTiempo()
    {
        time += Time.deltaTime;
        timeP.text = "Time " + (int)time;
    }*/
}

    


/* finalP = null;
        inicioP = null; */ 