using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Board : MonoBehaviour
{
    public int width, height, borderSize, score, scoreG=100, movimientos, movimientosF=0, prueba, tiempol;
    public GameObject tilePrefab, particulas,retry,next;
    public GameObject[] gamePiecesPrefabs;
    public float swapTime = .3f, time;
    Tile[,] m_allTiles;
    Gamepicereturn[,] m_allGamePieces;
    [SerializeField] Tile m_clickedTile, m_targetTile;
    bool m_playerInputEnabled = true;
    public bool activo = false;
    Transform tileParent;
    Transform gamePieceParent;
    public TextMeshProUGUI scoreP, timeP;
    public AudioSource objeto;
    public AudioClip sonidoNom, sonidoSlip;
    public bool sepuedemover;
    

    private void Start()
    {
        SetParents();
        m_allTiles = new Tile[width, height];
        m_allGamePieces = new Gamepicereturn[width, height];
        SetupTiles();
        SetupCamera();
        FillBoard(10, .5f);
    }//el comienzo y arranque de las matrices
  
    private void Update()
    {
        Puntaje();
        MedidorDeTiempo();
        CondicionGanar();
        scoreP.text = "Score " + score;
        while ((int)time<2)
        {
            score = 0;
            break;
        }
    }// comprobante de variaciones de datos durante el transcurso
    private void SetParents()
    {
        if (tileParent == null)
        {
            tileParent = new GameObject().transform;
            tileParent.name = "Tiles";
        tileParent.parent = this.transform;
        }
        if (gamePieceParent == null)
        {
            gamePieceParent = new GameObject().transform;
            gamePieceParent.name = "GamePieces";
        gamePieceParent = this.transform;
        }
    }// donde se ubican los tile y gamepice
    private void SetupCamera()

    {
        Camera.main.transform.position = new Vector3((float)(width - 1) / 2f, (float)(height - 1) / 2f, -10);

        float aspectRatio = (float)Screen.width / (float)Screen.height;
        float verticalSize = (float)height / 2f + (float)borderSize;
        float horizontalSize = ((float)width / 2f + (float)borderSize) / aspectRatio;
        Camera.main.orthographicSize = verticalSize > horizontalSize ? verticalSize :
        horizontalSize;
    }// ubica la camara dependiendo de los tama�os 
    private void SetupTiles()
    {
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                GameObject tile = Instantiate(tilePrefab, new Vector2(i, j), Quaternion.identity);
                tile.name = $"Tile((i),(3))";
                if (tileParent != null)
                {
                    tile.transform.parent = tileParent;
                }
                m_allTiles[i, j] = tile.GetComponent<Tile>();
                m_allTiles[i, j].Init(i, j, this);
                
            }
        }
    }// creacion de la matriz de tiles
    private void FillBoard(int falseOffset = 0, float moveTime = .1f)
    {
        List<Gamepicereturn> addedPieces = new List<Gamepicereturn>();
        for (int i = 0; i < width; i++)

        {
            for (int j = 0; j < height; j++)
            {
                if (m_allGamePieces[i, j] == null)
                {
                    if (falseOffset == 0)
                    {
                        Gamepicereturn piece = FillRandomAt(i, j);
                        addedPieces.Add(piece);
                    }
                    else
                    {
                        Gamepicereturn piece = FillRandomAt(i, j, falseOffset, moveTime);
                        addedPieces.Add(piece);
                    }
                }
            }
        }
        int maxIterations = 20;
        int iterations = 0;
        bool isFilled = false;
        while (!isFilled)
        {
            List<Gamepicereturn> matches = FindAllMatches();
            if (matches.Count == 0)
            {
                isFilled = true;
                break;
            }
            else
            {
                matches = matches.Intersect(addedPieces).ToList();

                if (falseOffset == 0)
                {
                    ReplaceWithRandom(matches);
                }
                else
                {
                    ReplaceWithRandom(matches, falseOffset, moveTime);
                }
            }
            if (iterations > maxIterations)
            {
                isFilled = true;
                Debug.LogWarning($"Board.FillBoard alcanzo el maximo de interacciones, abortar");
            }
            iterations++;
        }
        
    }// rellena la matriz
    public void ClickedTile(Tile tile)
    {
        if (m_clickedTile == null)
        {
            m_clickedTile = tile;
        }
    }// se�ala el tile
    public void DragToTile(Tile tile)
    {
        if (m_clickedTile != null && IsNextTo(tile, m_clickedTile))
        {
            m_targetTile = tile;
        }
    }//comprabante de si la puede mover
    public void ReleaseTile()
    {
        if (m_clickedTile != null && m_targetTile != null)
        {
            SwitchTiles(m_clickedTile, m_targetTile);
        }
        m_clickedTile = null;
        m_targetTile = null;
    }//  soltar de la pieza despues de comprobar si se puede mover
    private void SwitchTiles(Tile m_clickedTile, Tile m_targetTile)
    {
        AudioSource.PlayClipAtPoint(sonidoSlip, gameObject.transform.position);
        StartCoroutine(SwitchTilesRoutine(m_clickedTile, m_targetTile));
        movimientos--;
        if (movimientos <= movimientosF)
        {
            PerderPantalla();
        }
    }// movimiento de las fichas
    IEnumerator SwitchTilesRoutine(Tile clickedTile, Tile targetTile)
    {
        if (sepuedemover)
        {
            sepuedemover = false;
        }
        if (m_playerInputEnabled)
        {
            Gamepicereturn clickedPiece = m_allGamePieces[clickedTile.xIndex,
           clickedTile.yIndex];
            Gamepicereturn targetPiece = m_allGamePieces[targetTile.xIndex,
           targetTile.yIndex];
            if (clickedPiece != null && targetPiece != null)
            {
                clickedPiece.Move(targetTile.xIndex, targetTile.yIndex, swapTime);
                targetPiece.Move(clickedPiece.xIndex, clickedPiece.yIndex, swapTime);
                yield return new WaitForSeconds(swapTime);
                List<Gamepicereturn> clickedPieceMatches =
               FindMatchesAt(clickedTile.xIndex, clickedTile.yIndex);
                List<Gamepicereturn> targetPieceMatches = FindMatchesAt(targetTile.xIndex,
               targetTile.yIndex);
                if (clickedPieceMatches.Count == 0 && targetPieceMatches.Count == 0)
                {
                    clickedPiece.Move(clickedTile.xIndex, clickedTile.yIndex, swapTime);
                    targetPiece.Move(targetTile.xIndex, targetTile.yIndex, swapTime);
                    yield return new WaitForSeconds(swapTime);
                    sepuedemover = true;
                }
                else
                {
                    yield return new WaitForSeconds(swapTime);
                    CleatAndRefillBoard(clickedPieceMatches.Union(targetPieceMatches).ToList());
                }
            }
        }
    }// corutina del movimiento de las fichas
    private void CleatAndRefillBoard(List<Gamepicereturn> gamePieces)
    {
        StartCoroutine(ClearAndRefillRoutine(gamePieces));
    }// activador de la corutina que limpia y rellena 
    List<Gamepicereturn> FindMatches(int startX, int startY, Vector2 searchDirection, int minLenght = 3)
    {
        List<Gamepicereturn> matches = new List<Gamepicereturn>();
        Gamepicereturn startPiece = null;

        if (IsWithBounds(startX, startY))
        {
            startPiece = m_allGamePieces[startX, startY];
        }
        if (startPiece != null)
        {
            matches.Add(startPiece);
        }
        else
        {
            return null;
        }
        int nextX;
        int nextY;
        int maxValue = width > height ? width : height;
        for (int i = 1; i < maxValue; i++)
        {
            nextX = startX + (int)Mathf.Clamp(searchDirection.x, -1, 1) * i;
            nextY = startY + (int)Mathf.Clamp(searchDirection.y, -1, 1) * i;

            if (!IsWithBounds(nextX, nextY))
            {
                break;
            }
            Gamepicereturn nextPiece = m_allGamePieces[nextX, nextY];
            if (nextPiece == null)
            {
                break;
            }
            else
            {
                if (nextPiece.matchValue == startPiece.matchValue && !matches.Contains(nextPiece))
                {
                    matches.Add(nextPiece);
                }
                else
                {
                    break;
                }
            }
        }
        if (matches.Count >= minLenght)
        {
            return matches;
        }
        else
        {
            return null;
        }
    }// buscador de conjuntos o "matches"
    List<Gamepicereturn> FindVerticalMatches(int startX, int startY, int minLenght = 3)
    {
        List<Gamepicereturn> upwardMatches = FindMatches(startX, startY, Vector2.up, 2);
        List<Gamepicereturn> downwardMatches = FindMatches(startX, startY, Vector2.down, 2);
        if (upwardMatches == null)
        {
            upwardMatches = new List<Gamepicereturn>();
        }
        if (downwardMatches == null)
        {
            downwardMatches = new List<Gamepicereturn>();
        }
        var combinedMatches = upwardMatches.Union(downwardMatches).ToList();
        return combinedMatches.Count >= minLenght ? combinedMatches : null;
    }// buscador de matches en vertical
    List<Gamepicereturn> FindHorizontalMatches(int startX, int startY, int minLenght = 3)
    {
        List<Gamepicereturn> rightMatches = FindMatches(startX, startY, Vector2.right, 2);
        List<Gamepicereturn> leftMatches = FindMatches(startX, startY, Vector2.left, 2);

        if (rightMatches == null)
        {
            rightMatches = new List<Gamepicereturn>();
        }
        if (leftMatches == null)
        {
            leftMatches = new List<Gamepicereturn>();
        }
        var combinedMatches = rightMatches.Union(leftMatches).ToList();
        return combinedMatches.Count >= minLenght ? combinedMatches : null;
    }// buscador de matches en horizontal
    private List<Gamepicereturn> FindMatchesAt(int x, int y, int minlLenght = 3)
    {
        List<Gamepicereturn> horizontalMatches = FindHorizontalMatches(x, y, minlLenght);
        List<Gamepicereturn> verticalMatches = FindVerticalMatches(x, y, minlLenght);

        if (horizontalMatches == null)
        {
            horizontalMatches = new List<Gamepicereturn>();
        }
        if (verticalMatches == null)
        {
            verticalMatches = new List<Gamepicereturn>();
        }
        var combinedMatches = horizontalMatches.Union(verticalMatches).ToList();
        if (horizontalMatches.Count !=0 && verticalMatches.Count !=0)
        {
            for (int i = 0; i < combinedMatches.Count; i++)
            {
                score += 20;
                Debug.Log("encontro");
            }
        }
        return combinedMatches;
    }// encuentra los matches en la ubicacion especifica
    List<Gamepicereturn> FindMatchesAt(List<Gamepicereturn> gamePieces, int minLenght = 3)
    {
        List<Gamepicereturn> matches = new List<Gamepicereturn>();
        foreach (Gamepicereturn piece in gamePieces)
        {
            matches = matches.Union(FindMatchesAt(piece.xIndex, piece.yIndex, minLenght)).ToList();
        }
        return matches;
    }// sobre carga del anterior
    private bool IsNextTo(Tile start, Tile end)
    {
        if (Mathf.Abs(start.xIndex - end.xIndex) == 1 && start.yIndex == end.yIndex)
        {
            return true;
        }

        if (Mathf.Abs(start.yIndex - end.yIndex) == 1 && start.xIndex == end.xIndex)
        {
            return true;

        }
        return false;
    }// comprobante y limitante de que la ficha esta cerca y su movimiento sea solo de esa ficha cercana
    private List<Gamepicereturn> FindAllMatches()
    {
        List<Gamepicereturn> combinedMatches = new List<Gamepicereturn>();
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                var matches = FindMatchesAt(i, 3);
                combinedMatches = combinedMatches.Union(matches).ToList();
            }
        }
        return combinedMatches;
    }// encargado de encontrar todos los matches
    void HighlightTileO0ff(int x, int y)
    {

        SpriteRenderer spriteRender = m_allTiles[x, y].GetComponent<SpriteRenderer>();
        spriteRender.color = new Color(spriteRender.color.r, spriteRender.color.g, spriteRender.color.b, 0);



    }// encargado de apagar los tile despues de ser resaltados
    void HighlightTileOn(int x, int y, Color col)
    {
        SpriteRenderer spriteRenderer = m_allTiles[x, y].GetComponent<SpriteRenderer>();
        spriteRenderer.color = col;

    }// activa el resaltado de los tile
    void HighlightMatchesAt(int x, int y)
    {

        HighlightTileO0ff(x, y);

        var combinedMatches = FindMatchesAt(x, y);
        if (combinedMatches.Count > 0)

        {

            foreach (Gamepicereturn piece in combinedMatches)

            {
                HighlightTileOn(piece.xIndex, piece.yIndex, piece.GetComponent<SpriteRenderer>().color);

            }
        }
    }// resalta el macth en su ubicacion
    void HighlightMatches()
    {
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                HighlightMatchesAt(i, j);

            }
        }
    }// resalta los match
    void HighlightPieces(List<Gamepicereturn> gamePieces)
    {
        foreach (Gamepicereturn piece in gamePieces)

        {
            if (piece != null)

            {
                HighlightTileOn(piece.xIndex, piece.yIndex, piece.GetComponent<SpriteRenderer>().color);

            }
        }
    }// resalta las piezas
    void ClearPieceAt(int x, int y)
    {
        Gamepicereturn pieceToClear = m_allGamePieces[x, y];
        if (pieceToClear != null)
        {
            m_allGamePieces[x, y] = null;
            GameObject particula=Instantiate(particulas, pieceToClear.gameObject.transform.position, Quaternion.identity);
            Destroy(pieceToClear.gameObject);
            Destroy(particula, 1);
            AudioSource.PlayClipAtPoint(sonidoNom, gameObject.transform.position);
            prueba++;
        }
        HighlightTileO0ff(x, y);
    }// limpia las piezas del tablero
    void ClearPieceAt(List<Gamepicereturn> gamePieces)
    {
        foreach (Gamepicereturn piece in gamePieces)
        {
            if (piece != null)
            {
                ClearPieceAt(piece.xIndex, piece.yIndex);
            }
        }
    }// sobre carga del antrior
    void ClearBoard()
    {
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                ClearPieceAt(i, 3);
            }
        }
    }// limpiador del board
    GameObject GetRandomPiece()
    {

        int randomInx = Random.Range(0, gamePiecesPrefabs.Length);
        if (gamePiecesPrefabs[randomInx] == null)
        {
            Debug.LogWarning($"La clase Board en el array de prefabs en la posicion (randomInx) no contiene una pieza valida");
        }
        return gamePiecesPrefabs[randomInx];

    }// generador de piezas aleatorias
    public void PlaceGamePiece(Gamepicereturn gamePiece, int x, int y)
    {
        if (gamePiece == null)

        {
            Debug.LogWarning($"gamePiece invalida");
            return;
        }

        gamePiece.transform.position = new Vector2(x, y);
        gamePiece.transform.rotation = Quaternion.identity;

        if (IsWithBounds(x, y))

        {
            m_allGamePieces[x, y] = gamePiece;

        }

        gamePiece.SetCoord(x, y);
    }// ubicador de las piezas
    private bool IsWithBounds(int x, int y)
    {

        return (x >= 0 && x < width && y >= 0 && y < height);
    }// restringidor de las piezas
    Gamepicereturn FillRandomAt(int x, int y, int falseOffset = 0, float moveTime = .1f)
    {

        Gamepicereturn randomPiece = Instantiate(GetRandomPiece(), Vector2.zero, Quaternion.identity).GetComponent<Gamepicereturn>();
        if (randomPiece != null)

        {

            randomPiece.Init(this);
            PlaceGamePiece(randomPiece, x, y);

            if (falseOffset != 0)
            {

                randomPiece.transform.position = new Vector2(x, y + falseOffset);
                randomPiece.Move(x, y, moveTime);
            }
            randomPiece.transform.parent = gamePieceParent;

        }

        return randomPiece;

    }// llenado aleatorio de piezas
    void ReplaceWithRandom(List<Gamepicereturn> gamePieces, int falseOffset = 0, float moveTime = .1f)
    {
        foreach (Gamepicereturn piece in gamePieces)

        {

            ClearPieceAt(piece.xIndex, piece.yIndex);
            if (falseOffset == 0)
            {
                FillRandomAt(piece.xIndex, piece.yIndex);
            }
            else
            {
                FillRandomAt(piece.xIndex, piece.yIndex, falseOffset, moveTime);

            }
        }
    }// reubica las piezas de manera aleatoria
    List<Gamepicereturn> CollapseColumn(int column, float collapseTime = .1f)
    {
        List<Gamepicereturn> movingPieces = new List<Gamepicereturn>();

        for (int i = 0; i < height - 1; i++)
        {
            if (m_allGamePieces[column, i] == null)
            {
                for (int j = i + 1; j < height; j++)
                {
                    if (m_allGamePieces[column, j] != null)
                    {
                        m_allGamePieces[column, j].Move(column, i, collapseTime * (j - i));
                        m_allGamePieces[column, i] = m_allGamePieces[column, j];
                        m_allGamePieces[column, i].SetCoord(column, i);
                        if (!movingPieces.Contains(m_allGamePieces[column, i]))
                        {
                            movingPieces.Add(m_allGamePieces[column, i]);
                        }
                        m_allGamePieces[column, j] = null;
                        break;
                    }

                }
            }
        }
        return movingPieces;
    }// hace bajar las columnas llenando vacios
    List<Gamepicereturn> CollapseColumn(List<Gamepicereturn> gamePieces)
    {
        List<Gamepicereturn> movingPieces = new List<Gamepicereturn>();
        List<int> columnsToColapse = GetColumns(gamePieces);

        foreach (int column in columnsToColapse)

        {
            movingPieces = movingPieces.Union(CollapseColumn(column)).ToList();
        }
        return movingPieces;

    }// sobre carga de la anterior
    private List<int> GetColumns(List<Gamepicereturn> gamePieces)
    {

        List<int> columns = new List<int>();
        foreach (Gamepicereturn piece in gamePieces)
        {
            if (!columns.Contains(piece.xIndex))
            {


                columns.Add(piece.xIndex);

            }
        }
        return columns;
    }// ubicacion de columnas
    void ClearAndRefillBoard(List<Gamepicereturn> gamePieces)
    {
    }// limpiado y rellenado del board
    IEnumerator ClearAndRefillRoutine(List<Gamepicereturn> gamePieces)
    {
        m_playerInputEnabled = true;
        List<Gamepicereturn> matches = gamePieces;

        do
        {
            yield return StartCoroutine(ClearAndCollapseRoutine(matches));
            yield return null;
            yield return StartCoroutine(RefillRoutine());
            matches = FindAllMatches();
            yield return new WaitForSeconds(.5f);
        }
        while (matches.Count != 0);
        m_playerInputEnabled = true;
    }// corutina del limpiado y rellenado del board
    IEnumerator ClearAndCollapseRoutine(List<Gamepicereturn> gamePieces)
    {

        List<Gamepicereturn> movingPieces = new List<Gamepicereturn>();
        List<Gamepicereturn> matches = new List<Gamepicereturn>();
        HighlightPieces(gamePieces);

        yield return new WaitForSeconds(.5f);

        bool isFinished = false;

        while (!isFinished)
        {

            ClearPieceAt(gamePieces);
            yield return new WaitForSeconds(.25f);

            movingPieces = CollapseColumn(gamePieces);

            while (!IsCollapsed(gamePieces))
            {
                yield return null;
            }
            yield return new WaitForSeconds(.5f);
            matches = FindMatchesAt(movingPieces);

            if (matches.Count == 0)
            {
                isFinished = true;
                break;
            }
            else
            {
                yield return StartCoroutine(ClearAndCollapseRoutine(matches));
            }
        }
        yield return null;
    }// corutina del limpiado y reubicador de columnas
    IEnumerator RefillRoutine()
    {
        FillBoard(10, .3f);
        yield return null;
    }// corutina de rellenado
    private bool IsCollapsed(List<Gamepicereturn> gamePieces)
    {
        foreach (Gamepicereturn piece in gamePieces)
        {
            if (piece != null)
            {
                if (piece.transform.position.y - (float)piece.yIndex > 0.001f)
                {
                    return false;
                }
            }
        }
        return true;
    }// comprobador del colapso de las columnas
    void Puntaje()
    {
        if (prueba!=0)
        {
            if (prueba == 3)
            {
                score += 5;
            }
            if (prueba == 4)
            {
                score += 10;
            }
            if (prueba == 5)
            {
                score += 15;
            }
            if (prueba>=6)
            {
                score += 15*2; 
            }
            prueba = 0;
        }
          
    }// sumador de puntos por cantidad piezas en los matches
    void MedidorDeTiempo()
    {
        time += Time.deltaTime;       

        int min = Mathf.FloorToInt(time / 60);
        int seg = Mathf.FloorToInt(time % 60);
        timeP.text = string.Format("{00:00}:{01:00}", min, seg);

    }// controlador del tiempo que trasncurre durante los niveles
    void CondicionGanar()
    {
        if (score>= scoreG && movimientos>=movimientosF)
        {
            next.gameObject.SetActive(true);
            retry.gameObject.SetActive(false);      
        }
        if ((int)time>=tiempol)
        {
            PerderPantalla();
        }

    }// condiciones para ganar el juego variables por nivel
    
    void PerderPantalla()
    {
        SceneManager.LoadScene("lose");
    }// activador de la escena de perdida

   
}