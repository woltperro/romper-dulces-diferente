using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bola : MonoBehaviour
{
   /* public bool YaSeEjecuto=true;
    public float tiempoDeMovimiento;
    public int cordX, cordY;
    public TipoM TipoDeMovimiento;
    public AnimationCurve curve;
    public Board2 board;
    public Colorsito color;
    
    void Update()
    {
       
    }
    public void Coordenada(int x,int y)
    {
        cordX = x;
        cordY = y;
    }

    public void MoverPieza(int x, int y, float tiempoMovimiento)
    {
        
        if (YaSeEjecuto==true)
        {
            StartCoroutine(MoverPiece(new Vector3(x,y), tiempoMovimiento));
        }
        
    }

    IEnumerator MoverPiece(Vector3 posicionFinal, float tiempoMovimiento)
    {
        bool LlegoAlPunto = false;
        YaSeEjecuto = false;
        Vector3 posicionInicial = transform.position;
        float tiempoTranscurrido = 0;

        while (!LlegoAlPunto)
        {
            if (Vector3.Distance(transform.position, posicionFinal)<0.01f)
            {
                LlegoAlPunto = true;
                YaSeEjecuto = true;
                transform.position = new Vector3((int)posicionFinal.x,(int)posicionFinal.y,0);
                board.PiezasPosicion(this, (int)posicionFinal.x, (int)posicionFinal.y);
                break;
            }
            float t = tiempoTranscurrido / tiempoMovimiento;
            transform.position = Vector3.Lerp(posicionInicial, posicionFinal, t);
                      
            switch (tiempoDeMovimiento)
            {
                
            
                case (float)TipoM.Lineal:
                    t=curve.Evaluate(t);
                    break;
                case (float)TipoM.Entrada:
                    t = 1 - Mathf.Cos(t * Mathf.PI * .5f);
                    break;
                case (float)TipoM.Salida:
                    t = Mathf.Sin(t = Mathf.PI * .5f);
                    break;
                case (float)TipoM.Suavizado:
                    t = t * t * t * (3 - 2 * t);
                    break;
                case (float)TipoM.MasSuavizado:
                    t = t * t * t * t * (t * (t * 6 - 15) + 10);
                    break;
            }
            
            tiempoTranscurrido += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

    }
    public enum TipoM
    {
        Lineal,
        Entrada,
        Salida,
        Suavizado,
        MasSuavizado,
    }
    public enum Colorsito
    {
        Rojo,
        Verde,
        AzulOscuro,
        AzulClaro,
        Amarillo,
        Naranja,
        Morado,
        Magenta,


    }

    */
}
